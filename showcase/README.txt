This font showcases various debugging glyphs. It uses the Strokes
implementation, with the glyph program located in a generic function.


U+F000 (): Horizontal pixels per em
U+F001 (): Vertical pixels per em
U+F002 (): Horizontal point size
U+F003 (): Vertical point size
U+F007 (): GETINFO bit  0 > 0-7, Rasterizer version [1][3]
U+F008 (): GETINFO bit  1 >  8, Rotated glyph [1][3]
U+F009 (): GETINFO bit  2 >  9, Stretched glyph [1][3]
U+F00C (): GETINFO bit  5 > 12, Grayscale hinting [1][3]
U+F00D (): GETINFO bit  6 > 13, ClearType hinting [1][2][3]
U+F00E (): GETINFO bit  7 > 14, Compatible widths [1][2][3]
U+F00F (): GETINFO bit  8 > 15, Symmetrical smoothing [1][2], Vertical subpixels [3]
U+F010 (): GETINFO bit  9 > 16, BGR LCD stripes [1][2][3]
U+F011 (): GETINFO bit 10 > 17, Subpixel positioning [3]
U+F012 (): GETINFO bit 11 > 18, Symmetrical smoothing [3]
U+F013 (): GETINFO bit 12 > 19, Gray ClearType [3]


[1] http://www.microsoft.com/typography/otspec/ttinst.htm (TT Instruction Set Part 2)
[2] http://www.microsoft.com/typography/cleartype/truetypecleartype.aspx#Toc227035731
[3] http://git.savannah.gnu.org/cgit/freetype/freetype2.git/tree/src/truetype/ttinterp.c
