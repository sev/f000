TrueType debug glyphs
---------------------

The [DejaVu] fonts include an intriguing set of debugging glyphs, created
by Ben Laenen. They consist of 7-segment digits, controlled by TrueType
programs. Most notably, U+F000 displays the available pixels per em.

Based on that idea, I have implemented a few alternative, more efficient
and versatile variants from scratch. I share them in the hope that they
might be useful, or at least interesting, to others.


Implementations
---------------

_Stacks_\
My most efficient implementation that still displays somewhat legible digits.\
Vertices: 24, Program: 190B, Stack-depth: 48\
![Stacks](implementations/stacks/stacks.png)

_Strokes_\
A robust, reasonably efficient implementation that displays very clean digits.\
Vertices: 40, Program: 338B, Stack-depth: 90\
![Strokes](implementations/strokes/strokes.png)


Usage
-----

Copy the U+F000 outlines and program from the ttf file. Make sure
to set an appropriate maximum stack depth in your maxp table.

The outlines can be adjusted in proportion and size, as long as
horizontally or vertically parallel points retain their alignments.

To display a different value, replace the MPPEM instruction at the
beginning of the program.


Guiding Principles
------------------

_Completeness_\
All values from 00 to 99 should be supported.

_Correctness_\
The unhinted outlines should be distinct from any numerical value.
Overflows should be displayed with a distinct configuration.

_Independence_\
It should be easy to copy the program into any font. Font-specific
mechanisms like functions or control values should be avoided.

_Genericity_\
The displayed value should be pushed onto the stack once, at the very
beginning of the program. This way, the value can be changed easily,
and the remaining program can be moved into a generic function for reuse.

_Robustness_\
Different rendering modes should be supported.
Overlapping, reversed, or zero-width shapes should be avoided.

_Adaptability_\
Within reason and artistic license, it should be possible to adapt
size and proportions of the outlines to match other glyphs of a font.

_Efficiency_\
As little data as possible should be added to a font.


License
-------

This font software is made available under the terms of the
[Creative Commons CC0 1.0][CC0] Universal Public Domain Dedication.


[DejaVu]: https://dejavu-fonts.github.io/
[CC0]: https://creativecommons.org/publicdomain/zero/1.0/
